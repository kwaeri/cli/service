/**
 * SPDX-PackageName: kwaeri/service
 * SPDX-PackageVersion: 0.10.0
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


 'use strict'


// INCLUDES
import * as assert from 'assert';
import EventEmitter from 'events';
import { ServiceProvider, ExampleServiceProvider } from '../index.mjs';


// DEFINES
const emitter = new EventEmitter();
const esp = new ExampleServiceProvider( ServiceProvider.getEmitWrapper( emitter.emit.bind( emitter ) ) );


// SANITY CHECK - Makes sure our tests are working proerly
describe(
    'PREREQUISITE',
    () => {
        describe(
            'Sanity Test(s)',
            () => {

                it(
                    'Should return true.',
                    () => {
                        assert.equal( [1,2,3,4].indexOf(4), 3 );
                    }
                );

            }
        );
    }
);


describe(
    'Service Functionality Test Suite',
    () => {

        describe(
            'Get Service Type Test',
            () => {
                it(
                    'Should return true.',
                    async () => {
                        return Promise.resolve(
                            assert.equal(
                                esp.serviceType,
                                "Standard Service"
                            )
                        );
                    }
                );
            }
        );


        describe(
            'Get Service Provider Subsciptions Test',
            () => {
                it(
                    'Should return true.',
                    async () => {
                        return Promise.resolve(
                            assert.equal(
                                JSON.stringify(
                                    esp.getServiceProviderSubscriptions()
                                ),
                                JSON.stringify(
                                    { commands: {}, required: {}, optional: {}, subcommands: {} }
                                )
                            )
                        );
                    }
                );
            }
        );


        describe(
            'Get Service Providers Subscription Help Text Test',
            () => {
                it(
                    'Should return true.',
                    async () => {
                        return Promise.resolve(
                            assert.equal(
                                JSON.stringify(
                                    esp.getServiceProviderSubscriptionHelpText()
                                ),
                                JSON.stringify(
                                    {
                                        helpText: {
                                            "command": "To use this service, read this HelpText."
                                        }
                                    }
                                )
                            )
                        );
                    }
                );
            }
        );


        describe(
            'Set Service Event Metadata Test',
            () => {
                it(
                    'Should return true.',
                    async () => {
                        // REMOVE THIS, UNCOMMENT THE LINE BELOW IT, THEN UNCOMMENT TESTEVENTS METHOD IN EXAMPLESERVICEPROVIDER DEFINITION TO RESTORE
                        // PREVIOUS IMPLEMENTATION
                        emitter.on(
                            "ServiceEvent",
                            ( data: any ) => {
                                //DEBUG( `TestServiceEvent Caught and Handled!` );
                                //handler( data );
                                return Promise.resolve( assert.equal( JSON.stringify( data ), JSON.stringify( { progressLevel: 50, notice: "Complete", log: "Test", logType: 0 } ) ) )
                            }
                        );
                        //esp.testEvents( ( data: any ) => { return Promise.resolve( assert.equal( JSON.stringify( data ), JSON.stringify( { progressLevel: 50, notice: "Complete", log: "Test", logType: 0 } ) ) ) } );

                        esp.setServiceEventMetadata( { progressLevel: 50, notice: "Complete", log: "Test", logType: 0 } );
                    }
                );
            }
        );

    }
);
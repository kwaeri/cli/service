/**
 * SPDX-PackageName: kwaeri/service
 * SPDX-PackageVersion: 0.10.0
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


'use strict'


// INCLUDES
//import * as Events from 'events';
import debug from 'debug';


/* Configure Debug module support */
const DEBUG = debug( 'kue:service' );


// DEFINES

/* Symbols/Types for typing support of service provider types */
export type ServiceType = string;

export const SERVICE_TYPES = {
    STANDARD_SERVICE: "Standard Service",
    GENERATOR_SERVICE: "Generator Service",
    MIGRATOR_SERVICE: "Migrator Service",
    WIZARD_SERVICE: "Wizard Service"
};

/* Symbols/Types for typing support of common Service and Provider facilities */

export type ServiceEventPromise = {
    result: boolean;
};

export type ServiceEventBits = {
    progressLevel?: number;
    notice?: string;
    log?: string;
    logType?: number;
};

export type ServicePromiseBits = {
    result: boolean,
    type: string;
}

export type ServiceProviderSubscriptions = {
    commands: any;
    required: any;
    optional: any;
    subcommands?: any;
};

export type ServiceProviderHelpText = {
    helpText: any ;
};


/**
 * Service Interface
 *
 * The {@link BaseService} interface specifies the implementation expected by
 * all kue services, and defines how a subscriber can expect to contract a
 * publishers subscription.
 */
export interface BaseService {
    type: ServiceType;
    renderService<T>( options: any ): Promise<T>;
    get serviceType(): string;
    setServiceEventMetadata( data: ServiceEventBits ): void;
};


/**
 * ServiceProvider Interface
 *
 * The {@link BaseServiceProvider} interface specifies the implementation
 * expected by all publishers of kue services, and defines how a subscriber
 * can expect to discover a pubishers subscriptions.
 */
export interface BaseServiceProvider {
    getServiceProviderSubscriptions( options: any ): ServiceProviderSubscriptions;
    getServiceProviderSubscriptionHelpText<T extends ServiceProviderHelpText>( options: any ): T;
}


/**
 * Service Class
 *
 * The {@link Service} is the class from which all kue services should inherit
 * from.
 *
 * A service is a resource provided, typically through subsciption to a caller,
 * contracted when an end user requisitions the platform, and will [or should]
 * always need the Events class, and have at least some standard methods.
 *
 * This class helps to sensibly build a derived service provider, it will probably
 * only be used directly in special circumstances, and instead - developers would
 * extend from the {@link ServiceProvider} class.
 */
export abstract class Service implements BaseService {

    /**
     * @var { ServiceType }
     */
    type: ServiceType = SERVICE_TYPES.STANDARD_SERVICE;

    /**
     * @var { ( eventName: string | symbol, ...args: any[] ) => boolean }
     */
    protected emit?: ( ( ( data: ServiceEventBits ) => boolean ) | ( ( data: ServiceEventBits ) => void ) );

    /**
     * The Service class constructor
     * @param { Events.EventEmitter | ( ( data: ServiceEventBits ) => void ) } eventHandler An optional event handler will be composed
     *                                                                                      into the instance by the Steward, but can
     *                                                                                      be passed in directly by a caller (for
     *                                                                                      testing) - and is typed for obvious reasons
     *
     * **Example**
     *
     * A {@link ServiceProvider} could choose to leverage an event-based progress bar, and could `.bind()` to the `.emit()` method - the
     * instance of the *emitter* it's associated with. The emitter would define a **ServiceEvent** type and leverage the `Progress::
     * handler()` to handle those events:
     *
     * ```typescript
     * // Setup
     * const emitter = Events.EventEmitter,
     *      progress = new Progress();
     *
     * // Define the event, binding progress.handler to it
     * emitter.on( "ServiceEvent", progress.handler.bind( progress ) );
     *
     * // Create the service provider, passing in the event emitter's emit, using a special static wrapper method
     * const serviceProvider = new ServiceProvider( ServiceProvider.getEmitWrapper( emitter.emit.bind( emitter ) ) );
     *
     * // Or, leverage a composition-based alternative approach (similar to the steward)
     * const altServiceProvider = new ServiceProvider();
     * altServiceProvider.emit = ServiceProvider.getEmitWrapper( emitter.emit.bind( emitter ) );
     *
     * // Now any call to serviceProvider.updateProgress() is handled
     * serviceProvider.updateProgress( "TAG", { progressLevel: 50, notice: "Processing xyz ..." } );
     * altServiceProvider.updateProgress( "TAG", { progressLevel: 50, notice: "Processing xyz ..." } );
     * ```
     *
     * The other option a {@link ServiceProvider} could choose is to use the progress module directly, and leverage its `getHandler()`
     * convenience method to return a pre-bound handler reference:
     *
     * ```typescript
     * // Setup
     * const progress = new Progress(),
     *      serviceProvider = new ServiceProvider( progress.getHandler() );
     *
     * // Now any call to serviceProvider.updateProgress() is handled
     * ```
     *
     * There are merits to using an event based progress bar - so the option is supported in this fashion. However, a direct
     * route to updating the progress bar was considered - and included - because it does have a more responsive result.
     */
    constructor( handler?: ( ( data: ServiceEventBits ) => boolean ) | ( ( data: ServiceEventBits ) => void ) ) {

        if( handler )
            this.emit = handler;
    }

    /**
     * The getServiceType getter returns the string {@link ServiceType} of the {@link ServiceProvider}
     *
     * @returns { ServiceType }
     */
    get serviceType(): ServiceType {
        return this.type;
    }

    /**
     * Wraps `Events.EventEmitter.emit()` so that the signature matches the required signature for the
     * constructor's handler argument
     *
     * @param { ( eventName: string | symbol, ...args: any[] ) => boolean } emitFunc The `Events.EventEmitter.emit` being wrapped
     *
     * @returns { ( data: ServiceEventBits ) => void } A wrapped emit to service as a handler for progress bar events
     */
    static getEmitWrapper( emitFunc: ( ( eventName: string | symbol, ...args: any[] ) => boolean ) ): ( data: ServiceEventBits ) => void {
        return ( data: ServiceEventBits ) => emitFunc( "ServiceEvent", data );
    }

    /**
     * Renders the primary function of the {@link Service}.
     *
     * The generics scaffolding help to support advanced extended usage of the Service class. Please see
     * the existing ServiceProviders under the kwaeri scope to see how to get around the
     * generics requirements when you don't have a need for them.
     *
     * @param {NodeKitOptions|any} options An options object, typically an extension of {@link NodeKitOptions}
     *
     * @returns { T } Minimally, an object with a boolean result, and a string type. Empty template provided to allow for type limitation in derived classes..
     */
    abstract renderService<T>( options?: any ): Promise<T>;

    /**
     * Method to emit service event metadata for controlling
     * progress bar display
     *
     * @param { ServiceEventBits } data An object of {@link ServiceEventBits}
     */
    setServiceEventMetadata( data: ServiceEventBits): void {
        // Higher-Order Function fix
        //this.emit!( 'ServiceEvent', data );

        // Allows this change, avoiding if's ofc:
        this?.emit?.( data );
    }
}


/**
 * ServiceProvider Class
 *
 * The {@link ServiceProvider} class with which all kue service providers should extend from,
 * which inherits from the Events and {@link Service} classes and implements all required
 * interfaces for consistency.
 */
export abstract class ServiceProvider extends Service implements BaseServiceProvider {

    /**
     * Gets the subscription object for the service provider, used by the
     * Steward in order to reconcile user-provided commands with the available contracts
     * offered by the currently installed ServiceProviders.
     *
     * @param { any } options A redundant, future proofing, options argument.
     *
     * @return { ServiceProviderSubscriptions } A {@link ServiceProviderSubscriptions} object
     */
    abstract getServiceProviderSubscriptions( options: any ): ServiceProviderSubscriptions;

    /**
     * Gets help text for the {@link ServiceProvider}'s contracts/commands.
     *
     * @param { any} options A redundant, future proofing, options object
     *
     * @returns { ServiceProviderHelpText } An extension of {@link ServiceProviderHelpText}
     */
    abstract getServiceProviderSubscriptionHelpText<T extends ServiceProviderHelpText>( options: any ): T;

    /**
     * Method to simplify emitting progress bar events and debugging
     *
     * @param tag The tag to note for debugging purposes
     * @param serviceEventBits The progress bar event metadata to leverage in setting service event metadata
     *
     * @returns { void }
     */
     updateProgress( tag: String, serviceEventBits: ServiceEventBits ): void {
        DEBUG( `[${tag}] call 'setServiceEventMetadata' with  '${serviceEventBits}` );

        this.setServiceEventMetadata( serviceEventBits );
    }
}


// example of a basic service provider:
export class ExampleServiceProvider extends ServiceProvider {

    getServiceProviderSubscriptions( options?: any ): ServiceProviderSubscriptions {
        return { commands: {}, required: {}, optional: {}, subcommands: {} };
    }

    getServiceProviderSubscriptionHelpText<T extends ServiceProviderHelpText>( options?: any ): T {
        return { helpText: { "command": `To use this service, read this HelpText.` } } as T;
    }

    async renderService<T extends any>( options?: any ): Promise<T> {
        this.setServiceEventMetadata( { progressLevel: 50, notice: "Test Notice", log: "Test Log", logType: 1 } )

        return Promise.resolve( ( () => console.log( 'Service should run at this point.' ) ) as T );
    }
}